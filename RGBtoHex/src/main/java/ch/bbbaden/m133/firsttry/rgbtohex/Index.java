/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.firsttry.rgbtohex;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author jusch
 */
@Named(value = "index")
@SessionScoped
public class Index implements Serializable {
    
    private String r, g, b;
    private Map<String,Object> rgb_values;

    public Index() {
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
    
    public Map getRGBValues(){
        rgb_values = new LinkedHashMap<String,Object>();
        for(int i=0;i<=255;i++){
            rgb_values.put(i+"", i);
        }
        return rgb_values;
    }
    
    public String getHex(){
        RGBController.setRGB(r, g, b);
        return RGBController.getHex();
    }
}
