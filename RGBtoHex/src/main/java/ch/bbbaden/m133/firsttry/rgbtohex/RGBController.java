/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.firsttry.rgbtohex;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class RGBController {
    
    private static String r, g, b;

    public static void setRGB(String r, String g, String b) {
        if(r.equals("")){
            RGBController.r = "0";
        }else{
            RGBController.r = r;
        }
        
        if(g.equals("")){
            RGBController.g = "0";
        }else{
            RGBController.g = g;
        }
        
        if(b.equals("")){
            RGBController.b = "0";
        }else{
            RGBController.b = b;
        }
    }
    
    public RGBController() {
    }
    
    public static String getHex(){
        if(r == null){
            return "#FFFFFF";
        }else{
            String h_r = Integer.toHexString(Integer.parseInt(r));
            String h_g = Integer.toHexString(Integer.parseInt(g));
            String h_b = Integer.toHexString(Integer.parseInt(b));
            return "#"+r+g+b;
        }
    }
}
