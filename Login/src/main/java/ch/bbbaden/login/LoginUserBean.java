/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.login;



import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Alexander Flick
 */
@Named(value="loginUserBean")
@SessionScoped
public class LoginUserBean implements Serializable {

    private String username;
    private String password;
    private boolean loggedIn;

    public String doLogin() {        
        if (LoginCheck.check(username, password)) {
            this.loggedIn = true;
            ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).changeSessionId();
            return "/secured/welcome?faces-redirect=true";
        }else{
            return "/start";
        }
    }

    public boolean isLoggedIn() {
        return this.loggedIn;
    }
    
    public String doLogout(){
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
        
        this.loggedIn = false;
        return "/start";
    }

    // Getters & Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
