/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.login;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author jusch
 */
public class LoginCheck {
    
    private static String xmlSource = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF\\users.xml";

    
    public static boolean check(String username, String password){
        SAXBuilder jdomBuilder = new SAXBuilder();
        try {
            Document jdomDocument = (Document) jdomBuilder.build(xmlSource);
            Element root = jdomDocument.getRootElement();
            
            List<Element> users = root.getChildren();
            for(Element curr : users){
                String e_username = curr.getChildText("Username");
                String e_password = curr.getChildText("Password");
                if(e_username.equals(username) && e_password.equals(password)){
                    return true;
                }
            }
        } catch (JDOMException ex) {
            Logger.getLogger(LoginCheck.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LoginCheck.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
}
