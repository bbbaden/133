package ch.bbbaden.m133.guestbook;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.faces.context.FacesContext;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author jusch
 */
public class GuestBookEntryList {

    private final static String xmlSource = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "WEB-INF\\entries.xml";
    private DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMAN);

    private static GuestBookEntryList instance;

    private ArrayList<Entry> entries = new ArrayList<>();
    private Element root;

    private GuestBookEntryList() {
        retrieveData();
    }

    private void retrieveData() {
        entries.clear();
        SAXBuilder jdomBuilder = new SAXBuilder();
        try {
            Document jdomDocument = (Document) jdomBuilder.build(xmlSource);
            root = jdomDocument.getRootElement();

            List<Element> entries = root.getChildren();
            for (Element curr : entries) {
                int id = Integer.parseInt(curr.getChildText("id"));
                String name = curr.getChildText("name");
                Date time = df.parse(curr.getChildText("time"));
                String body = curr.getChildText("body");

                this.entries.add(new Entry(name, body, time, id));
            }
        } catch (JDOMException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static GuestBookEntryList getInstance() {
        if (instance == null) {
            instance = new GuestBookEntryList();
        }
        return instance;
    }

    public List<Entry> getList() {
        return entries;
    }

    public void addEntry(String name, String body) {
        Date curr = new Date();
        int id = Integer.parseInt(root.getChildren().get(root.getChildren().size() - 1).getChildText("id")) + 1;

        Element child = new Element("entry");
        child.addContent((new Element("id")).setText(id + ""));
        child.addContent((new Element("name")).setText(name + ""));
        child.addContent((new Element("time")).setText(df.format(curr) + ""));
        child.addContent((new Element("body")).setText(body + ""));
        root.addContent(child);
        saveRoot();
        retrieveData();
    }

    public void saveRoot() {
        try {
            FileWriter writer = new FileWriter(xmlSource);
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            outputter.output(root, writer);
            outputter.output(root, System.out);
            writer.close(); // close writer
        } catch (IOException ex) {

        }
    }
}
