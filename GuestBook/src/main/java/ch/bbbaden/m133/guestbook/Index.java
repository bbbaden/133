package ch.bbbaden.m133.guestbook;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author jusch
 */
@Named(value = "index")
@SessionScoped
public class Index implements Serializable {

    private String name, body;

    public Index() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void send() {
        GuestBookEntryList.getInstance().addEntry(name, body);
        name = "";
        body = "";
    }

    public List<Entry> getEntries() {
        return GuestBookEntryList.getInstance().getList();
    }

    public List<Entry> getEntriesOrderNewOld() {
        List<Entry> list = GuestBookEntryList.getInstance().getList();
        Collections.sort(list, new Comparator<Entry>() {
            @Override
            public int compare(Entry o1, Entry o2) {
                if (o1.getTime().after(o2.getTime())) {
                    return -1;
                } else if (o1.getTime().before(o2.getTime())) {
                    return 1;
                }
                return 0;
            }
        });
        return list;
    }

}
