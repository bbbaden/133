/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.firsttry.index;

import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author jusch
 */
@Named(value = "index")
@Dependent
public class Index {

    /**
     * Creates a new instance of Index
     */
    public Index() {
    }
    
    public String getName(){
        return "Julian Schmuckli";
    }
}
