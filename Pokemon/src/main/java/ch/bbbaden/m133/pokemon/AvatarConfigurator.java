/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.pokemon;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jusch
 */
@Named(value = "avatarConfigurator")
@RequestScoped
public class AvatarConfigurator {
    
    private HttpSession session;
    private Avatar avatar;

    public AvatarConfigurator() {
        FacesContext fCtx = FacesContext.getCurrentInstance();
        session = (HttpSession) fCtx.getExternalContext().getSession(false);
        if(session.getAttribute("avatar")==null){
            session.setAttribute("avatar", new Avatar());
        }
    }
    
    public String getAvatarSkin(){
        try{
            switch(getAvatar().getSkin()){
                case BLACK:
                    return "black";
                case WHITE:
                    return "white";
            }
            return "not_defined";
        }catch(Exception ex){
            return ex.getMessage();
        }
    }
    
    public void setAvatarSkin(String skin){
        avatar = getAvatar();
        if(skin.equals("BLACK")){
            avatar.setSkin(AvatarSkin.BLACK);
        }else if(skin.equals("WHITE")){
            avatar.setSkin(AvatarSkin.WHITE);
        }else{
            throw new NullPointerException();
        }
        saveAvatar(avatar);
    }
    
    public String getAvatarEyeColor(){
        try{
            switch(getAvatar().getEyeColor()){
                case GREEN:
                    return "Grün";
                case BLUE:
                    return "Blau";
            }
            return "not_defined";
        }catch(Exception ex){
            return ex.getMessage();
        }
    }
    
    public List getAvatarEyeColorList(){
        List values = new ArrayList<String>();
        values.add("Grün");
        values.add("Blau");
        return values;
    }
    
    public String getImg(){
        switch(getAvatar().getSkin()){
            case BLACK:
                return "d";
            case WHITE:
                return "h";
        }
        return "d";
    }
    
    public void setAvatarEyeColorList(Object color){
        List<String> values = getAvatarEyeColorList();
        for(String curr : values){
            if(curr.equals(color)){
                avatar = getAvatar();
                if(color.equals("Blau")){
                    avatar.setEyeColor(AvatarEyeColor.BLUE);
                }else if(color.equals("Grün")){
                    avatar.setEyeColor(AvatarEyeColor.GREEN);
                }else{
                    throw new NullPointerException();
                }
                saveAvatar(avatar);
                break;
            }
        }
    }
    
    private Avatar getAvatar(){
        return (Avatar) session.getAttribute("avatar");
    }
    
    private void saveAvatar(Avatar avatar){
        session.setAttribute("avatar", avatar);
    }
}
