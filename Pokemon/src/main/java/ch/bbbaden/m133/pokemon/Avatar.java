/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.pokemon;

/**
 *
 * @author jusch
 */
public class Avatar {
    private AvatarSkin skin;
    private AvatarEyeColor eyeColor;
    
    public Avatar(){
        
    }

    public AvatarSkin getSkin() {
        return skin;
    }

    public void setSkin(AvatarSkin skin) {
        this.skin = skin;
    }

    public AvatarEyeColor getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(AvatarEyeColor eyeColor) {
        this.eyeColor = eyeColor;
    }
    
}
