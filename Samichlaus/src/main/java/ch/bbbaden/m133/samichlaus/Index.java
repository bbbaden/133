/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.samichlaus;

import ch.bbbaden.m133.samichlaus.product.Product;
import ch.bbbaden.m133.samichlaus.product.ProductsHolder;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author jusch
 */
@Named(value = "index")
@RequestScoped
public class Index {
    
    private ArrayList<Product> products;
    private Product active_product;
    
    public Index() {
        
    }
    
    public List<Product> getProducts(){
        products = ProductsHolder.getInstance().getProducts();
        return products;
    }
    
    public void setActiveProduct(Product product){
        active_product = product;
    }

    public Product getActiveProduct() {
        return active_product;
    }
    
    
}
