/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.bbbaden.m133.samichlaus.product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author jusch
 */
public class ProductsHolder {
    private static final String XML_SOURCE = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/") + "assets\\xml\\products.xml";
    private static ProductsHolder instance;
    private ArrayList<Product> products = new ArrayList<>();
    
    private ProductsHolder(){
        SAXBuilder jdomBuilder = new SAXBuilder();
        try {
            Document jdomDocument = (Document) jdomBuilder.build(XML_SOURCE);
            Element root = jdomDocument.getRootElement();
            
            List<Element> products = root.getChildren();
            for(Element curr : products){
                int id = Integer.parseInt(curr.getChildText("id"));
                float price = Float.parseFloat(curr.getChildText("price"));
                String name = curr.getChildText("name");
                String description = curr.getChildText("description");
                String img = curr.getChildText("img");
                
                Product product = new Product(id, name, description, img, price);
                this.products.add(product);
            }
        } catch (JDOMException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public ArrayList<Product> getProducts(){
        return products;
    }
    
    public Product getProduct(int id){
        for(Product curr : products){
            if(curr.getId()==id){
                return curr;
            }
        }
        return null;
    }
    
    public static ProductsHolder getInstance(){
        if(instance==null){
            instance = new ProductsHolder();
        }
        return instance;
    }
}
